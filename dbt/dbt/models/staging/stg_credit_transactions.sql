WITH source AS (
    SELECT * 
    FROM {{ ref('raw_credit_transactions') }}
),

renamed AS (

    SELECT
        id as customer_id,
        first_name,
        last_name

    FROM source

)

SELECT * FROM renamed