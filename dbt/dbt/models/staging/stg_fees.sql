{{ 
    config(
        materialized='incremental',
        unique_key='booking_id',
        indexes=[
            {'columns': ['id'], 'type': 'btree'},
            {'columns': ['id'], 'unique': True}
        ],
    ) 
}}

WITH selected_bookings AS (
    SELECT b.id AS booking_id
    FROM {{ ref('stg_bookings') }} b
    --filter incremental
    {% if not is_incremental() or var('scan_type', 'UPDATED_AT') == 'IS_ALL'%}
        WHERE 1 = 1

    {% else %}
        WHERE b.datamart_created_at > (select max(datamart_created_at) from {{ this }})

    {% endif %}
     ),
    commission AS (
    SELECT sb.booking_id,
        COALESCE(-b.original_commission_before_overried,0) AS original_commission_amount,
        (CASE
            WHEN b.override_commission_value IS NOT NULL
                AND ct IS NOT NULL THEN coalesce(ct.amount,0)
            ELSE b.override_commission_value
        END) AS new_commission_amount
    FROM selected_bookings sb
    JOIN {{ ref('stg_bookings')}} b ON b.id = sb.booking_id
    LEFT JOIN
        (SELECT ct.booking_id,
                ct.amount
        FROM selected_bookings sb
        JOIN {{ ref('stg_credit_transactions') }} ct ON sb.booking_id = ct.booking_id
        JOIN
        (SELECT MAX(ct.id) AS max_id
         FROM selected_bookings sb
         JOIN {{ ref('stg_credit_transactions') }} ct ON ct.booking_id = sb.booking_id
         WHERE ct.reason_type = 9
           AND ct.status =1
         GROUP BY ct.booking_id) mi ON mi.max_id = ct.id
      AND ct.sub_category = 3) ct ON ct.booking_id = sb.booking_id),
    location_fee AS (
    SELECT l.booking_id,
        SUM(CASE
                WHEN waiting_time_confirmed = TRUE THEN waiting_time_fees
                ELSE 0
            END) AS waiting_time_fees,
        SUM(CASE
                WHEN tolls_confirmed = TRUE THEN tolls_fees
                ELSE 0
            END) AS tolls_fees,
        SUM (CASE
                WHEN parking_confirmed = TRUE THEN parking_fees
                ELSE 0
            END) AS parking_fees
    FROM selected_bookings sb
    JOIN {{ ref('stg_locations') }} l ON l.booking_id = sb.booking_id
        AND l.deleted_at IS NULL
    GROUP BY l.booking_id),
    custom_reimbursement AS (
    SELECT sb.booking_id,
        SUM(CASE
                WHEN r.is_confirmed=TRUE THEN r.fees
                ELSE 0
            END) AS custom_reimbursement_fees
    FROM selected_bookings sb
    LEFT JOIN {{ ref('stg_locations') }} l ON l.booking_id = sb.booking_id
    LEFT JOIN {{ ref('stg_reimbursements') }} r ON r.location_id = l.id
    WHERE l.deleted_at IS NULL
    GROUP BY sb.booking_id),
    surcharge_adjustment AS (
    SELECT bs.booking_id,
            (surcharge_infos::json->>'surcharge_adjustment')::float AS surcharge_adjustment
    FROM selected_bookings sb
    JOIN {{ ref('stg_booking_surcharges') }} bs ON bs.booking_id = sb.booking_id),
    fee AS (
    SELECT sb.booking_id,
        -b.override_commission_value AS override_commission,
        b.cod_pod_payback_amount,
        b.driver_earning,
        COALESCE(comm.original_commission_amount,0) AS original_commission_amount,
        COALESCE(comm.new_commission_amount,0) AS new_commission_amount,
        cr.custom_reimbursement_fees,
        lf.waiting_time_fees + lf.tolls_fees + lf.parking_fees + cr.custom_reimbursement_fees AS reimbursement,
        COALESCE(sa.surcharge_adjustment,0) AS surcharge_adjustment,
        b.driver_earning - (lf.waiting_time_fees + lf.tolls_fees + lf.parking_fees + cr.custom_reimbursement_fees) - COALESCE(sa.surcharge_adjustment,0) AS booking_fee
    FROM selected_bookings sb
    JOIN {{ ref('stg_bookings') }} b ON b.id = sb.booking_id
    LEFT JOIN commission comm ON comm.booking_id = sb.booking_id
    LEFT JOIN location_fee lf ON lf.booking_id = sb.booking_id
    LEFT JOIN custom_reimbursement cr ON cr.booking_id = sb.booking_id
    LEFT JOIN surcharge_adjustment sa ON sa.booking_id = sb.booking_id),
 bonus_value AS (
    SELECT sb.booking_id,
        b.is_confirmed,
        CASE
            WHEN b.is_confirmed = TRUE THEN bb.amount
            ELSE (CASE
                    WHEN bb.pricing_type = 1
                            AND bb.based_on = 0 THEN f.original_commission_amount * bb.pricing_value /100
                    WHEN bb.pricing_type = 1
                            AND bb.based_on = 1 THEN f.booking_fee * bb.pricing_value / 100
                    WHEN bb.pricing_type = 0 THEN pricing_value
                END)
        END AS bonus_value,
        bb.bonus_min_cap,
        bb.bonus_max_cap
    FROM selected_bookings sb
    JOIN {{ ref('stg_bookings') }} b ON b.id = sb.booking_id
    JOIN {{ ref('stg_booking_bonuses') }} bb ON sb.booking_id = bb.booking_id
    LEFT JOIN fee f ON f.booking_id = sb.booking_id),
    driver_security_bond AS (
    SELECT sb.booking_id, - ct.amount AS driver_security_bond
    FROM selected_bookings sb
    JOIN {{ ref('stg_credit_transactions') }} ct ON sb.booking_id = ct.booking_id
        AND ct.reason_type = 21
        AND ct.status = 1),
    RESULT AS (
    SELECT sb.booking_id,
        COALESCE(f.driver_earning,0) AS driver_earning,
        COALESCE(f.override_commission,0) AS override_commission,
        COALESCE(f.surcharge_adjustment,0) AS surcharge_adjustment,
        COALESCE(f.cod_pod_payback_amount,0) AS cod_pod_payback_amount,
        COALESCE(f.original_commission_amount,0) AS original_commission_amount,
        COALESCE(f.new_commission_amount,0) AS new_commission_amount,
        COALESCE(dsb.driver_security_bond,0) AS driver_security_bond,
        COALESCE(CASE
                    WHEN bv.is_confirmed = TRUE THEN bv.bonus_value
                    ELSE (CASE
                                WHEN (bv.bonus_min_cap IS NOT NULL
                                    AND bv.bonus_value < bv.bonus_min_cap) THEN bv.bonus_min_cap
                                WHEN (bv.bonus_max_cap IS NOT NULL
                                    AND bv.bonus_value > bv.bonus_max_cap) THEN bv.bonus_max_cap
                                ELSE bv.bonus_value
                            END)
                END,0) AS driver_bonus
    FROM selected_bookings sb
    LEFT JOIN fee f ON f.booking_id = sb.booking_id
    LEFT JOIN driver_security_bond dsb ON dsb.booking_id = sb.booking_id
    LEFT JOIN bonus_value bv ON bv.booking_id = sb.booking_id)
SELECT DISTINCT booking_id,
                driver_earning,
                override_commission,
                surcharge_adjustment,
                cod_pod_payback_amount,
                original_commission_amount,
                new_commission_amount,
                driver_security_bond,
                driver_bonus,
                CASE
                    WHEN override_commission > 0 THEN driver_earning + override_commission + cod_pod_payback_amount - driver_security_bond + driver_bonus
                    ELSE driver_earning - original_commission_amount + cod_pod_payback_amount - driver_security_bond + driver_bonus
                END AS driver_net_pay
FROM RESULT